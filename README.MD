https://gitlab.com/troitsa/jse15
# JAVA EE 8 APPLICATION 

## SOFTWARE

- Java JDK 1.8 (Oracle HotSpot)
- Maven 4

## BUILD APPLICATION WITH MAVEN
```bash
mvn clean install
```
## RUN SERVER

```bash
java -jar tm-server/target/release/bin/tm-server.jar
```

## RUN CLIENT

```bash
java -jar tm-client/target/release/bin/tm-client.jar
```

## DEVELOPER

> Rose Vlasova <nikarose@ya.ru>