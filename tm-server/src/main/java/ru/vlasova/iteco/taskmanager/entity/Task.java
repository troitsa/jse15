package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "entity.cache")
public final class Task extends AbstractEntity {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @ManyToOne
    private Project project;

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @NotNull
    @Column(updatable = false)
    private Date dateCreate = new Date(System.currentTimeMillis());

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @Override
    @NotNull
    public String toString() {
        return "    Task " + name;
    }

}
