package ru.vlasova.iteco.taskmanager.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    @NotNull
    private final EntityManager em;

    public TaskRepository(@NotNull final EntityManager em) {
        this.em = em;
    }

    @Override
    @NotNull
    public List<Task> getTasksByProjectId(@NotNull final String userId,
                                          @NotNull final String projectId) {
        return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId",
                Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).getResultList();
    }

    @Override
    @NotNull
    public List<Task> search(@NotNull final String userId, @NotNull final String searchString) {
        return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId AND " +
                "(t.name LIKE :searchString OR t.description LIKE :searchString)", Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("searchString", "%" + searchString + "%")
                .setParameter("searchString", "%" + searchString + "%").getResultList();
    }

    @Override
    @NotNull
    public List<Task> findAllByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId",
                Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).getResultList();
    }

    @Override
    @Nullable
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return em.createQuery("SELECT t FROM app_task t WHERE t.id = :id AND t.user.id = :userId",
                Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("id", id).setParameter("userId", userId).setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeByUserId(@NotNull final String userId, @NotNull final String id) {
        em.createQuery("DELETE FROM app_task t WHERE t.id = :id AND t.user.id = :userId")
                .setParameter("id", id).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        em.createQuery("DELETE FROM app_task t WHERE t.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String id) {
        return em.find(Task.class, id);
    }

    @Override
    public void persist(@NotNull final Task task) {
        em.persist(task);
    }

    @Override
    public void merge(@NotNull final Task task) {
        em.merge(task);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM app_task").executeUpdate();
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return em.createQuery("SELECT t FROM app_task t", Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void remove(@NotNull final String id) {
        em.createQuery("DELETE FROM app_task t WHERE t.id = :id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    @NotNull
    public List<Task> sortTask(@Nullable final String userId,
                               @Nullable final String sortMode) {
        if (sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId ORDER BY " +
                            "t.dateCreate", Task.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
                case ("2"):
                    return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId ORDER BY " +
                            "t.dateStart", Task.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
                case ("3"):
                    return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId ORDER BY " +
                            "t.dateFinish", Task.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
                case ("4"):
                    return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId ORDER BY " +
                            "t.status", Task.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
            }
        }
        return em.createQuery("SELECT t FROM app_task t WHERE t.user.id= :userId ORDER BY " +
                "t.name", Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).getResultList();
    }

}
