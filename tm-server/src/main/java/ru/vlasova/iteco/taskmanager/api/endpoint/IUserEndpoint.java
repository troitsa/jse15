package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.dto.UserDTO;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO insertUser(@Nullable final String login,
                       @Nullable final String password) throws Exception;

    @Nullable
    @WebMethod
    UserDTO doLogin(@Nullable final String login,
                 @Nullable final String password) throws Exception;

    @Nullable
    @WebMethod
    String checkUser(@Nullable final String token,
                     @Nullable final String login) throws Exception;

    @WebMethod
    void editUser(@Nullable final String token,
                  @Nullable final UserDTO userDto,
                  @Nullable final String login,
                  @Nullable final String password) throws Exception;

    @Nullable
    @WebMethod
    List<UserDTO> findAllUsers(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    UserDTO findUserBySession(@Nullable final String token,
                           @Nullable String id) throws Exception;

    @Nullable
    @WebMethod
    UserDTO findUser(@Nullable final String id) throws Exception;

    @WebMethod
    void persistUser(@Nullable UserDTO user) throws Exception;

    @WebMethod
    void mergeUser(@Nullable final String token,
                   @Nullable UserDTO user) throws Exception;

    @WebMethod
    void removeUser(@Nullable final String token,
                    @Nullable String id) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final String token) throws Exception;

    @WebMethod
    Boolean checkRole(@Nullable final String userId,
                      @NotNull final List<Role> roles) throws Exception;

}
