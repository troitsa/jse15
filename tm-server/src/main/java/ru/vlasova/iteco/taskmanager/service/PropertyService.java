package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static ru.vlasova.iteco.taskmanager.constant.DataConstant.DIR_RESOURCE;

public final class PropertyService implements IPropertyService {

    @NotNull
    final Properties properties = new Properties();

    @NotNull
    final FileInputStream fileInputStream = new FileInputStream(DIR_RESOURCE + File.separator + "app.properties");

    public PropertyService() throws IOException {
        properties.load(fileInputStream);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        @NotNull final String sessionSalt = properties.getProperty("session.salt");
        return sessionSalt;
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        @NotNull final String sessionCycle = properties.getProperty("session.cycle");
        return Integer.parseInt(sessionCycle);
    }

    @NotNull
    @Override
    public Integer getSessionLifetime() {
        @NotNull final String sessionLifetime = properties.getProperty("session.lifetime");
        return Integer.parseInt(sessionLifetime);
    }

    @NotNull
    @Override
    public String getSecretKey() {
        @NotNull final String secretKey = properties.getProperty("token.secretkey");
        return secretKey;
    }

}
