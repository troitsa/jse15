package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;

import java.util.List;

public interface IProjectRepository {

    @Nullable
    String getIdByIndex(@NotNull final String userId, int projectIndex);

    @NotNull
    List<Project> search(@NotNull final String userId, @NotNull final String searchString);

    @NotNull
    List<Project> findAllByUserId(@NotNull final String userId);

    void removeAllByUserId(@NotNull final String userId);

    @Nullable
    Project findOneByUserId(@NotNull final String userId, @NotNull final String id);

    void remove(@NotNull final String userId, @NotNull final String id);

    void removeById(@NotNull final String id);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOne(@NotNull final String id);

    void persist(@NotNull final Project obj);

    void merge(@NotNull final Project obj);

    void removeAll();

    @NotNull
    List<Project> sortProject(@Nullable final String userId,
                              @Nullable final String sortMode);

}
