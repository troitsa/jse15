package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    String getToken (@Nullable final String login, @Nullable String password) throws Exception;

    @Nullable
    Session create(@Nullable final String login, @Nullable final String pass) throws Exception;

    void validate(@Nullable final String token) throws Exception;

    @Nullable
    List<Session> findAll();

    @Nullable
    Session findOne(@Nullable final String id);

    void persist(@Nullable final Session obj);

    void merge(@Nullable final Session obj);

    void remove(@Nullable final String token) throws Exception;

    void removeAll();

    boolean contains(@Nullable final String sessionId);

    void checkSession(@Nullable final String token, @NotNull final Role role) throws Exception;

    @Nullable
    String getCurrentUserId(@Nullable final String token) throws Exception;
}
