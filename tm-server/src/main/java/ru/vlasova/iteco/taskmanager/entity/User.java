package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "entity.cache")
public final class User extends AbstractEntity {

    @OnDelete(action = OnDeleteAction.CASCADE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Project> projectList = new ArrayList<>();

    @OnDelete(action = OnDeleteAction.CASCADE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Task> taskList = new ArrayList<>();

    @OnDelete(action = OnDeleteAction.CASCADE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Session> sessionList = new ArrayList<>();

    @Id
    @NotNull
    private String id;

    @Nullable
    @Column(unique = true, nullable = false)
    private String login = "";

    @Nullable
    private String pwd = "";

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    public User(@NotNull final Role role) {
        this.role = role;
    }

    public User(@NotNull final String login, @NotNull final String pwd) {
        this.id =  UUID.nameUUIDFromBytes(login.getBytes()).toString();
        this.login = login;
        this.pwd = HashUtil.MD5(pwd);
    }

    @Override
    @NotNull
    public String toString() {
        return "User: " + login + ", " + role;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @Nullable final User user = (User) o;
        if (login == null) return false;
        return login.equals(user.getLogin());
    }

}
