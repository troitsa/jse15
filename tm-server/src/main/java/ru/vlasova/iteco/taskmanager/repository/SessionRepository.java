package ru.vlasova.iteco.taskmanager.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager em;

    public SessionRepository(@NotNull EntityManager em) {
        this.em = em;
    }

    @Override
    public @NotNull List<Session> findAll() {
        return em.createQuery("SELECT s FROM app_task s", Session.class)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String id) {
        return em.find(Session.class, id);
    }

    @Override
    public void remove(@NotNull final String id) {
        em.createQuery("DELETE FROM app_session s WHERE s.id = :id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public void persist(@NotNull final Session session) {
        em.persist(session);
    }

    @Override
    public void merge(@NotNull final Session session) {
        em.merge(session);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM app_session").executeUpdate();
    }

    @Override
    public void removeSessionBySignature(@NotNull final String signature) {
        em.createQuery("DELETE FROM app_session s WHERE s.signature = :signature")
                .setParameter("signature", signature).executeUpdate();
    }

    @Override
    public boolean contains(@NotNull final String id) {
        @Nullable final Session session = em.find(Session.class, id);
        return session != null;
    }

}
