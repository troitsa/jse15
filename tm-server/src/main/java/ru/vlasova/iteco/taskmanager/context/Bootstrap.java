package ru.vlasova.iteco.taskmanager.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.*;
import ru.vlasova.iteco.taskmanager.endpoint.ProjectEndpoint;
import ru.vlasova.iteco.taskmanager.endpoint.SessionEndpoint;
import ru.vlasova.iteco.taskmanager.endpoint.TaskEndpoint;
import ru.vlasova.iteco.taskmanager.endpoint.UserEndpoint;
import ru.vlasova.iteco.taskmanager.service.*;

import javax.xml.ws.Endpoint;
import java.io.IOException;

public class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final EntityManagerService entityManagerService = new EntityManagerService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final PropertyService propertyService =  new PropertyService();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public Bootstrap() throws IOException {
    }

    public void start() throws Exception {
        entityManagerService.init();
        Endpoint.publish("http://localhost:8080/project?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/task?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/user?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/session?wsdl", sessionEndpoint);
    }

}
