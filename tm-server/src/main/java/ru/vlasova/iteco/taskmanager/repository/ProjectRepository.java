package ru.vlasova.iteco.taskmanager.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager em;

    public ProjectRepository(@NotNull final EntityManager em) {
        this.em = em;
    }

    @Override
    @Nullable
    public String getIdByIndex(@NotNull final String userId, int projectIndex) {
        @Nullable final List<Project> projectList = findAllByUserId(userId);
        @Nullable final Project project = projectList.get(projectIndex);
        if (project == null) return null;
        @NotNull final String projectId = project.getId();
        return projectId;
    }

    @Override
    @NotNull
    public List<Project> search(@NotNull final String userId, @NotNull final String searchString) {
        return em.createQuery("SELECT p FROM app_project p WHERE p.user.id= :userId AND " +
                "(p.name LIKE :searchString OR p.description LIKE :searchString)", Project.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("searchString", "%" + searchString + "%")
                .setParameter("searchString", "%" + searchString + "%").getResultList();
    }

    @Override
    @NotNull
    public List<Project> findAllByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT p FROM app_project p WHERE p.user.id= :userId",
                Project.class).setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).getResultList();
    }

    @Override
    @Nullable
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return em.createQuery("SELECT p FROM app_project p WHERE p.id = :id AND p.user.id = :userId",
                Project.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) {
        em.createQuery("DELETE FROM app_project p WHERE p.id = :id AND p.user.id = :userId")
                .setParameter("id", id).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeById(@NotNull final String id) {
        em.createQuery("DELETE FROM app_project p WHERE p.id = :id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        em.createQuery("DELETE FROM app_project p WHERE p.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String id) {
        return em.find(Project.class, id);
    }

    @Override
    public void persist(@NotNull final Project project) {
        em.persist(project);
    }

    @Override
    public void merge(@NotNull final Project project) {
        em.merge(project);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM app_project ").executeUpdate();
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return em.createQuery("SELECT p FROM app_project p", Project.class)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Project> sortProject(@Nullable final String userId,
                                     @Nullable final String sortMode) {
        if (sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    return em.createQuery("SELECT p FROM app_project p WHERE p.user.id= :userId ORDER BY " +
                            "p.dateCreate", Project.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
                case ("2"):
                    return em.createQuery("SELECT p FROM app_project p WHERE p.user.id= :userId ORDER BY " +
                            "p.dateStart", Project.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
                case ("3"):
                    return em.createQuery("SELECT p FROM app_project p WHERE p.user.id= :userId ORDER BY " +
                            "p.dateFinish", Project.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
                case ("4"):
                    return em.createQuery("SELECT p FROM app_project p WHERE p.user.id= :userId ORDER BY " +
                            "p.status", Project.class)
                            .setHint(QueryHints.CACHEABLE, true)
                            .setParameter("userId", userId).getResultList();
            }
        }
        return em.createQuery("SELECT p FROM app_project p WHERE p.user.id= :userId ORDER BY " +
                "p.name", Project.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).getResultList();
    }

}
