package ru.vlasova.iteco.taskmanager.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager em;

    public UserRepository(@NotNull final EntityManager em) {
        this.em = em;
    }

    @NotNull
    @Override
    public String checkUser(@NotNull final String login) {
        @Nullable final User user = em.createQuery("SELECT u FROM app_user u WHERE u.login = :login",
                User.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("login", login).setMaxResults(1)
                .getSingleResult();
        return user.getId();
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) {
         List list = em.createQuery("SELECT u FROM app_user u WHERE  u.id = :id",
                User.class)
                 .setHint(QueryHints.CACHEABLE, true)
                 .setParameter("id", id).setMaxResults(1).getResultList();
        return list.isEmpty() ? null : (User)list.get(0);
    }

    @Override
    public void persist(@NotNull final User user) {
        em.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        em.merge(user);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM app_user ").executeUpdate();
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return em.createQuery("SELECT u FROM app_user u", User.class)
                .setHint(QueryHints.CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void remove(@NotNull final String id) {
        em.createQuery("DELETE FROM app_user u WHERE u.id = :id")
                .setParameter("id", id).executeUpdate();
    }

}