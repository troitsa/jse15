package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.dto.TaskDTO;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    public TaskDTO insertTask(@Nullable final String token,
                           @Nullable final String userId,
                           @Nullable final String name,
                           @Nullable final String description,
                           @Nullable final String dateStart,
                           @Nullable final String dateFinish) throws Exception {
        validateSession(token);
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        return toTaskDTO(serviceLocator.getTaskService().insert(user, name, description, dateStart, dateFinish));
    }

    @Override
    public @Nullable TaskDTO getTaskByIndex(@Nullable final String token,
                                         @Nullable final String userId,
                                         int index) throws Exception {
        validateSession(token);
        return toTaskDTO(serviceLocator.getTaskService().getTaskByIndex(userId, index));
    }

    @Override
    public @Nullable List<TaskDTO> getTasksByProjectId(@Nullable final String token,
                                                    @Nullable final String userId,
                                                    @Nullable final String projectId) throws Exception {
        validateSession(token);
        return serviceLocator.getTaskService()
                .getTasksByProjectId(userId, projectId)
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void removeTaskByUserId(@Nullable final String token,
                                   @Nullable final String userId,
                                   @Nullable final String id) throws Exception {
        validateSession(token);
        serviceLocator.getTaskService().remove(userId, id);
    }

    @Override
    public void removeTaskByIndex(@Nullable final String token,
                                  @Nullable final String userId,
                                  int index) throws Exception {
        validateSession(token);
        serviceLocator.getTaskService().remove(userId,index);
    }

    @Override
    public @Nullable List<TaskDTO> findAllTasks(@Nullable final String token) throws Exception {
        validateSession(token);
        return serviceLocator.getTaskService().findAll()
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable List<TaskDTO> findAllTasksByUserId(@Nullable final String token,
                                                     @Nullable final String userId) throws Exception {
        validateSession(token);
        return serviceLocator.getTaskService()
                .findAll(userId).stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable TaskDTO findOneTask(@Nullable final String token,
                                      @Nullable final String id) throws Exception {
        validateSession(token);
        return toTaskDTO(serviceLocator.getTaskService().findOne(id));
    }

    @Override
    public @Nullable TaskDTO findOneTaskByUserId(@Nullable final String token,
                                              @Nullable final String userId,
                                              @Nullable final String id) throws Exception {
        validateSession(token);
        return toTaskDTO(serviceLocator.getTaskService().findOneByUserId(userId, id));
    }

    @Override
    @Nullable
    public void persistTask(@Nullable final String token,
                            @Nullable final TaskDTO task) throws Exception {
        validateSession(token);
        serviceLocator.getTaskService().persist(toTask(task));
    }

    @Override
    public void mergeTask(@Nullable final String token,
                          @Nullable final TaskDTO task) throws Exception {
        validateSession(token);
        serviceLocator.getTaskService().merge(toTask(task));
    }

    @Override
    public void removeTask(@Nullable final String token,
                           @Nullable final String id) throws Exception {
        validateSession(token);
        serviceLocator.getTaskService().remove(id);
    }

    @Override
    public void removeAllTasks(@Nullable final String token) throws Exception {
        validateSession(token);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    public void removeAllTasksByUserId(@Nullable final String token,
                                       @Nullable final String userId) throws Exception {
        validateSession(token);
        serviceLocator.getTaskService().removeAll(userId);
    }

    @Override
    @NotNull
    public List<TaskDTO> searchTask(@Nullable final String token,
                                    @Nullable final String userId,
                                    @Nullable final String searchString) throws Exception {
        validateSession(token);
        return serviceLocator.getTaskService()
                .search(userId, searchString)
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<TaskDTO> sortTask(@Nullable final String userId,
                                  @Nullable final String sortMode) {
        return serviceLocator.getTaskService()
                .sortTask(userId, sortMode)
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    private Task toTask(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return null;
        @Nullable final Task task = new Task();
        task.setId(taskDTO.getId());
        User user = serviceLocator.getUserService().findOne(taskDTO.getUserId());
        if (user == null) return null;
        task.setUser(user);
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateCreate(taskDTO.getDateCreate());
        task.setDateStart(taskDTO.getDateStart());
        task.setDateFinish(taskDTO.getDateFinish());
        task.setStatus(taskDTO.getStatus());
        return task;
    }

    @Nullable
    private TaskDTO toTaskDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateCreate(task.getDateCreate());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }
}