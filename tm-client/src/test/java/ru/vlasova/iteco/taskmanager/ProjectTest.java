package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.vlasova.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProjectTest extends AbstractTest {

    @After
    public void after() throws Exception {
        taskEndpoint.removeAllTasksByUserId(tokenUser, userId);
        taskEndpoint.removeAllTasksByUserId(tokenAdmin, adminId);
        projectEndpoint.removeAllProjectByUserId(tokenUser, userId);
        projectEndpoint.removeAllProjectByUserId(tokenAdmin, adminId);
    }

    @Test
    public void findAllProjects() throws Exception {
        @NotNull final ProjectDTO project1 = createProject(userId);
        @NotNull final ProjectDTO project2 = createProject(adminId);
        @NotNull final ProjectDTO project3 = createProject(userId);
        project3.setName("Project 3");
        projectEndpoint.persistProject(tokenUser, project1);
        projectEndpoint.persistProject(tokenAdmin, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        @NotNull final List<ProjectDTO> userProjects = projectEndpoint.findAllProjectsByUserId(tokenUser, userId);
        @NotNull final List<ProjectDTO> adminProjects = projectEndpoint.findAllProjectsByUserId(tokenUser, adminId);
        Assert.assertTrue(userProjects.size() + adminProjects.size() == 3);
    }

    @Test
    public void findAllProjectsByUserId() throws Exception {
        @NotNull final ProjectDTO project1 = createProject(userId);
        @NotNull final ProjectDTO project2 = createProject(userId);
        @NotNull final ProjectDTO project3 = createProject(userId);
        project3.setName("Project 3");
        projectEndpoint.persistProject(tokenUser, project1);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        @NotNull final List<ProjectDTO> userProjects = projectEndpoint.findAllProjectsByUserId(tokenUser, userId);
        Assert.assertTrue(userProjects.size() == 3);
    }

    @Test
    public void  findOneProject() throws Exception {
        @NotNull final ProjectDTO project = createProject(userId);
        projectEndpoint.persistProject(tokenUser, project);
        assertEquals(project.getName(), projectEndpoint.findOneProject(tokenUser, project.getId()).getName());
    }

    @Test
    public void  findOneProjectByUserId() throws Exception {
        @NotNull final ProjectDTO project = createProject(userId);
        projectEndpoint.persistProject(tokenUser,project);
        assertEquals(project.getName(), projectEndpoint.findOneProjectByUserId(tokenUser,
                userId, project.getId()).getName());
    }

    @Test
    public void persistProject() throws Exception {
        @NotNull final ProjectDTO project = createProject(userId);
        projectEndpoint.persistProject(tokenUser,project);
        Assert.assertNotNull(projectEndpoint.findOneProject(tokenUser, project.getId()));
    }

    @Test
    public void mergeProject() throws Exception {
        @NotNull final ProjectDTO project = createProject(userId);
        project.setName("testtest");
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull ProjectDTO testProject = projectEndpoint.findOneProject(tokenUser, project.getId());
        testProject.setDescription("test");
        projectEndpoint.mergeProject(tokenUser, testProject);
        Assert.assertEquals("test", testProject.getDescription());
    }

    @Test
    public void removeProjectById() throws Exception {
        @NotNull final ProjectDTO project = createProject(userId);
        projectEndpoint.persistProject(tokenUser,project);
        @NotNull ProjectDTO testProject = projectEndpoint.findOneProject(tokenUser, project.getId());
        Assert.assertEquals("TestProject", projectEndpoint.findOneProject(tokenUser, project.getId()).getName());
        projectEndpoint.removeProjectById(tokenUser, testProject.getId());
        Assert.assertNull(projectEndpoint.findOneProject(tokenUser, project.getId()));
    }

    @Test
    public void removeProjectByUserId() throws Exception {
        @NotNull final ProjectDTO project = createProject(userId);
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull ProjectDTO testProject = projectEndpoint.findOneProject(tokenUser, project.getId());
        Assert.assertEquals("TestProject", projectEndpoint.findOneProject(tokenUser, project.getId()).getName());
        projectEndpoint.removeProjectByUserId(tokenUser, userId, testProject.getId());
        Assert.assertNull(projectEndpoint.findOneProject(tokenUser, project.getId()));
    }

    @Test
    public void removeAllProjects() throws Exception {
        @NotNull final ProjectDTO project1 = createProject(userId);
        @NotNull final ProjectDTO project2 = createProject(userId);
        @NotNull final ProjectDTO project3 = createProject(userId);
        project3.setName("Project 3");
        projectEndpoint.persistProject(tokenUser, project1);
        projectEndpoint.persistProject(tokenAdmin, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        @NotNull final List<ProjectDTO> userProjects = projectEndpoint.findAllProjectsByUserId(tokenUser, userId);
        Assert.assertTrue(userProjects.size() == 3);
        projectEndpoint.removeAllProjectByUserId(tokenUser, userId);
        @NotNull final List<ProjectDTO> userProjects1 = projectEndpoint.findAllProjectsByUserId(tokenUser, userId);
        Assert.assertTrue(userProjects1.size() == 0);
    }

    @Test
    public void getTasksByProjectIndex() throws Exception {
        @NotNull final ProjectDTO project1 = createProject(userId);
        @NotNull final TaskDTO task1 = new TaskDTO();
        task1.setName("TestTask");
        task1.setProjectId(project1.getId());
        task1.setUserId(userId);
        projectEndpoint.persistProject(tokenUser, project1);
        taskEndpoint.persistTask(tokenUser, task1);
        @NotNull final List<TaskDTO> tasks = projectEndpoint.getTasksByProjectIndex(tokenUser, userId, 0);
        Assert.assertTrue(tasks.size() == 1);
    }

    @Test
    public void searchProject() throws Exception {
        @NotNull final ProjectDTO project1 = createProject(userId);
        @NotNull final ProjectDTO project2 = createProject(userId);
        @NotNull final ProjectDTO project3 = createProject(userId);
        project3.setName("Lorem");
        project2.setDescription("Lorem ipsum");
        projectEndpoint.persistProject(tokenUser, project1);
        projectEndpoint.persistProject(tokenAdmin, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        @NotNull final List<ProjectDTO> projectList = projectEndpoint.searchProject(tokenUser, userId, "Lorem");
        Assert.assertTrue(projectList.size() == 2);
    }

    @Test
    public void sortProject() throws Exception {
        @NotNull final ProjectDTO project1 = createProject(userId);
        @NotNull final ProjectDTO project2 = createProject(userId);
        @NotNull final ProjectDTO project3 = createProject(userId);
        project3.setName("ALorem");
        project2.setName("BLorem");
        project1.setName("CLorem");
        projectEndpoint.persistProject(tokenUser, project1);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        @NotNull final List<ProjectDTO> sortProjects = projectEndpoint.sortProject(userId, "");
        Assert.assertEquals("ALorem", sortProjects.get(0).getName());
        Assert.assertEquals("BLorem", sortProjects.get(1).getName());
        Assert.assertEquals("CLorem", sortProjects.get(2).getName());
    }

    private ProjectDTO createProject(String userId) throws Exception {
        @NotNull final ProjectDTO project = projectEndpoint.insertProject(tokenUser, userId, "TestProject",
                "Description 123", "10.01.2002", "15.10.2005");
        return project;
    }

}