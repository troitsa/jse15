package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import ru.vlasova.iteco.taskmanager.api.endpoint.*;
import ru.vlasova.iteco.taskmanager.endpoint.ProjectEndpointService;
import ru.vlasova.iteco.taskmanager.endpoint.SessionEndpointService;
import ru.vlasova.iteco.taskmanager.endpoint.TaskEndpointService;
import ru.vlasova.iteco.taskmanager.endpoint.UserEndpointService;

import java.lang.Exception;

public abstract class AbstractTest {

    @Nullable
    protected static String tokenUser;

    @Nullable
    protected static String tokenAdmin;

    @Nullable
    protected static String userId;

    @Nullable
    protected static String adminId;

    @Nullable
    protected static IProjectEndpoint projectEndpoint;

    @Nullable
    protected static ISessionEndpoint sessionEndpoint;

    @Nullable
    protected static ITaskEndpoint taskEndpoint;

    @Nullable
    protected static IUserEndpoint userEndpoint;

    @BeforeClass
    public static void setUp() throws Exception {
        projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
        sessionEndpoint =  new SessionEndpointService().getSessionEndpointPort();
        taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        userEndpoint = new UserEndpointService().getUserEndpointPort();

        @NotNull final UserDTO user = userEndpoint.insertUser("testUser","test");
        userEndpoint.persistUser(user);
        userId = user.getId();
        tokenUser = sessionEndpoint.getToken("testUser","test");

        @NotNull final UserDTO admin = userEndpoint.insertUser("testAdmin","test");
        admin.setRole(Role.ADMIN);
        userEndpoint.persistUser(admin);
        adminId = admin.getId();
        tokenAdmin = sessionEndpoint.getToken("testAdmin","test");
    }

    @AfterClass
    public static void afterClass() throws Exception {
//        sessionEndpoint.removeSession(tokenUser);
//        sessionEndpoint.removeSession(tokenAdmin);
        userEndpoint.removeUser(tokenUser, userId);
        userEndpoint.removeUser(tokenAdmin, adminId);
    }
}